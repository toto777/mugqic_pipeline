[default]
clusterSubmitCmd=msub
clusterSubmitCmdSuffix= | grep \"[0-9]\"
clusterWalltime=-l walltime=24:00:0
clusterCPU=-l nodes=1:ppn=1
clusterOtherArg=-V -m ae -M $JOB_MAIL -W umask=0002
clusterQueue=-q sw
clusterWorkDirArg=-d
clusterOutputDirArg=-j oe -o
clusterJobNameArg=-N
clusterCmdProducesJobId=true
clusterDependencyArg=-W x=depend:afterok:
clusterDependencySep=:
referenceFasta=/software/areas/genomics/genomes/Homo_sapiens/hg19/fasta/hg19.fasta
referenceSequenceDictionary=/software/areas/genomics/genomes/Homo_sapiens/hg19/fasta/hg19.fasta.dict
referenceMappabilityBedIndexed=/software/areas/genomics/genomes/Homo_sapiens/hg19/annotations/mappabilityGC/Illu_PE.exclusion.bed.gz
referenceSnpEffGenome=hg19
igvGenome=hg19
dbSnp=/software/areas/genomics/genomes/Homo_sapiens/hg19/annotations/dbsnp137.hg19.vcf
knownSites=/software/areas/genomics/genomes/Homo_sapiens/hg19/annotations/dbsnp137.hg19.vcf.gz
dbNSFP=/software/areas/genomics/genomes/Homo_sapiens/hg1k_v37/annotations/dbNSFP2.0/dbNSFP2.0.txt
extraJavaFlags=-XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304
tmpDir=/localscratch/
rawReadDir=raw_reads

## sould be experimentType="wholeGenome" for WGS metrics
experimentType="wholeGenome"
## prefix identifier for job  
projectName="DNAseq"

moduleVersion.trimmomatic=mugqic/trimmomatic/0.30
moduleVersion.bwa=mugqic/bwa/0.7.6a
moduleVersion.bvatools=mugqic/bvatools/1.2
moduleVersion.java=mugqic/java/oracle-jdk1.7.0_15
moduleVersion.picard=mugqic/picard/1.107
moduleVersion.samtools=mugqic/samtools/0.1.19-gpfs
moduleVersion.gatk=mugqic/GenomeAnalysisTK/2.7-2
moduleVersion.igvtools=mugqic/igvtools/2.3.14
moduleVersion.vcftools=mugqic/vcftools/0.1.11
moduleVersion.tabix=mugqic/tabix/0.2.6
moduleVersion.snpeff=mugqic/snpEff/3.4
moduleVersion.tools=mugqic/tools/1.6
moduleVersion.cranR=mugqic/R/3.0.2
moduleVersion.python=mugqic/python/2.7.6

[trim]
# Don't do trimming of any kind
#skip=1
nbThreads=6
minQuality=30
minLength=50
adapterFile=adapters-truseq.fa
clipSettings=:2:30:15
# To keep overlapping pairs use the following
# clipSettings=:2:30:15:8:true

clusterWalltime=-l walltime=24:00:0
clusterCPU=-l nodes=1:ppn=6

[aln]
# aligner can be 'mem' or 'backtrack'
# for mem, a 'mem' section is needed
# for 'backtrack' uncomment this section
aligner=mem
#bwaAlnThreads=12
#bwaExtraSamXeFlags=-T -t 11
#bwaRefIndex=/software/areas/genomics/genomes/Homo_sapiens/hg19/fasta/bwa/hg19.fasta
#bwaInstitution=McGill University and Genome Quebec Innovation Center
#sortRam=15G
#sortRecInRam=3750000
#clusterWalltime=-l walltime=24:00:0
#clusterCPU=-l nodes=1:ppn=12

[mem]
bwaExtraFlags=-M -t 7
bwaRefIndex=/software/areas/genomics/genomes/Homo_sapiens/hg19/fasta/bwa/hg19.fasta
bwaInstitution=McGill University and Genome Quebec Innovation Center
sortRam=15G
sortRecInRam=3750000
clusterWalltime=-l walltime=24:00:0
clusterCPU=-l nodes=1:ppn=8

[mergeFiles]
mergeRam=10G
mergeRecInRam=2000000

[mergeLanes]
clusterWalltime=-l walltime=72:00:0
clusterCPU=-l nodes=1:ppn=6

[indelRealigner]
realignRam=11G
realignReadsInRam=2500000
nbRealignJobs=10
clusterWalltime=-l walltime=60:00:0
clusterCPU=-l nodes=1:ppn=6

[mergeRealign]
clusterWalltime=-l walltime=72:00:0
clusterCPU=-l nodes=1:ppn=6

[fixmate]
fixmateRam=24G
fixmateRecInRam=5750000
clusterWalltime=-l walltime=96:00:0
clusterCPU=-l nodes=1:ppn=8

[markDup]
markDupRam=4G
markDupRecInRam=1000000
clusterWalltime=-l walltime=48:00:0
clusterCPU=-l nodes=1:ppn=2

[recalibration]
threads=12
recalRam=30G
clusterWalltime=-l walltime=72:00:0
clusterCPU=-l nodes=1:ppn=12

[collectMetrics]
collectMetricsRam=4G
collectMetricsRecInRam=1000000
clusterWalltime=-l walltime=48:00:0
clusterCPU=-l nodes=1:ppn=1

[depthOfCoverage]
# you can set it implicitly, leave blank for whole genome or set auto which uses the sampleSheet to identify the bed file.
coverageTargets=auto
# maxDepth is RAM limited. maxDepth * 8 * nbIntervals ~= RAM needed
extraFlags=--gc --maxDepth 1000 --minMappingQuality 15 --minBaseQuality 15 --ommitN --threads 2
percentThresholds=10,25,50,75,100,500,1000
ram=13G
clusterWalltime=-l walltime=48:00:0
clusterCPU=-l nodes=1:ppn=3

[genomeCoverage]
genomeCoverageRam=4G
percentThresholds=10,25,50,75,100,500
extraJavaFlags=-XX:ParallelGCThreads=2
clusterWalltime=-l walltime=96:00:0
clusterCPU=-l nodes=1:ppn=2

[targetCoverage]
coverageRam=8G
percentThresholds=10,25,50,75,100,500
extraJavaFlags=-XX:ParallelGCThreads=2
coverageTargets=/software/areas/genomics/genomes/Homo_sapiens/hg19/annotations/CCDS.hg19.bed
clusterWalltime=-l walltime=96:00:0
clusterCPU=-l nodes=1:ppn=2

[computeTDF]
clusterWalltime=-l walltime=96:00:0
clusterCPU=-l nodes=1:ppn=1

[mpileup]
mpileupExtraFlags=-L 1000 -E -q 1 -D -S -g -u
approxNbJobs=300
clusterWalltime=-l walltime=96:00:0
clusterCPU=-l nodes=1:ppn=2

[mergeFilterBCF]
varfilterExtraFlags=-d 2 -D 1200 -Q 15
clusterWalltime=-l walltime=24:00:0
clusterCPU=-l nodes=1:ppn=1

[rawmpileup]
mpileupExtraFlags=-d 1000 -E -q 1 -Q 0
clusterWalltime=-l walltime=96:00:0
clusterCPU=-l nodes=1:ppn=2

[annotateDbSnp]
siftRam=8G
extraJavaFlags=-XX:ParallelGCThreads=2
clusterCPU=-l nodes=1:ppn=2

[annotateDbNSFP]
siftRam=8G
extraJavaFlags=-XX:ParallelGCThreads=2
clusterCPU=-l nodes=1:ppn=2

[computeEffects]
extraJavaFlags=-XX:ParallelGCThreads=1
snpeffRam=2G
#snpeffParams=
#referenceSnpEffGenome=

[sortQname]

[countTelomere]

[fullPileup]
